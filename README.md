# Touch Inspiration Interview Project

  

Thank you for taking the time to complete this exercise. We're excited that you're considering joining our amazing team.

For consideration, **please complete the following project in [Nestjs](https://nestjs.com/).

**This is a backend quiz, an existing frontend will consume your endpoints**


The objective of this assignment, is to create a simple money tracker for users. Money can be tracked in multiple wallets (accounts), for example, if a user has multiple businesses. When a user's profile is pulled up, it should show a overall balance and all their wallets (accounts) and their respective balances. Drilling down into each wallet (account), should show the detailed transactions of that wallet (account).  

**User Flow:**

 1. A user signs up (no sign in required. Disregard authentication).

 2. A user can create wallets.

 3. A user can add expenses or incomes to a wallet.

 4. A user can view their profile with the summary of wallets and balances. Show overall balance of all wallets and balance of each wallet.

 5. A user can drill into each wallet to see balance and transactions.

**Assessment Criteria**

 1. Clean, readable, well thought out code.

 2. DRY code.

 3. Correct implementation of a wallet/ledger.

 4. Correct database table relations

 5. Multiple & descriptive commits, preferably a commit per feature. 

 6. Speed
